<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class KategoriController extends Controller
{
    public function daftar_kategori()
    {
        return view('admin/kategori', [
            'title' => 'Kategori',
            'kategori' => Kategori::all()
        ]);
    }


    public function tambah_kategori(Request $request)
    {
        $paket = Kategori::create($request->except(['token', 'submit']));
        if($request->has(('gambar'))){
            $request->file('gambar')->move('assets/img/upload/', $request->file('gambar')->getClientOriginalName());
            $paket->gambar = $request->file('gambar')->getClientOriginalName();
            $paket->save();
        }
        if ($paket->save()) {
            return redirect('/kategori')->with('tambah_kategori', 'Kategori Berhasil Ditambah!');
        };
    }

    public function delete_kategori($id)
    {
        Kategori::find($id)->delete();
        return redirect()->back()->with("delete_kategori","kategori Berhasil di Hapus");
    }


    public function update_kategori($id)
    {
        return view('admin/update/update_kategori', [
            'title' => 'Update Kategori',
            'kategori'=> Kategori::find($id)
        ]);
    }

    public function edit_kategori(Request $request)
    {
        // dd($request->file('gambar'));
        $paket = Kategori::find($request->id);
        $paket->nama_kategori = $request->nama_kategori;
        $paket->deskripsi = $request->deskripsi;

        $request->validate([
            'gambar' => 'required|image|mimes:pjeg,png,jpg,gif,svg',
         ]);
        // Periksa apakah ada file gambar yang diunggah
        if ($request->has('gambar')) {
            // Hapus gambar lama jika ada
            if ($paket->gambar) {
                Storage::delete('assets/img/upload/' . $paket->gambar);
            }

            $request->file('gambar')->move('assets/img/upload/', $request->file('gambar')->getClientOriginalName());
            $paket->gambar = $request->file('gambar')->getClientOriginalName();
        }


        if ($paket->save()) {
            return redirect('/kategori')->with("edit_kategori", "Berhasil Diupdate!");
        } else {
            // Handle the case where the save fails
            return redirect('/kategori')->with("edit_kategori", "Gagal Diupdate!");
        }
    }

}
