<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    public function index()
    {
        $kategori = Kategori::all();

        return view('landing', [
            'title' => 'Landing',
            'kategori' => $kategori
        ]);
    }

    public function layanan()
    {
        return view('layanan', [
            'title' => 'Layanan',
        ]);
    }

    public function login(Request $request)
    {
        $request->validate(
            [
                'email' => 'required',
                'password' => 'required'
            ],
            [
                'email.required' => 'email wajib diisi',
                'password.required' => 'password wajib diisi',
            ]
        );

        $infologin = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if (Auth::attempt($infologin)) {
            // Jika berhasil login, dapatkan role pengguna
            $role = Auth::user()->role;

            // Arahkan pengguna berdasarkan peran (role)
            if ($role == 'admin') {
                return redirect('/admin');
            } elseif ($role == 'customer') {
                return redirect('/');
            } else {
                // Tambahkan logika lain jika ada peran lainnya
                return redirect('/');
            }
        } else {
            return back()->with('loginError', 'Login Gagal, Silahkan Masukkan Username dan Password yang Benar! ');
        }

    }

    public function tambah_user(Request $request)
    {
        $user=new User;
        $user->nama=$request->nama;
        $user->no_tlp=$request->no_tlp;
        $user->email=$request->email;
        $user->password= Hash::make($request->password);
        $user->role=$request->role;
        $user->save();

        return redirect('/')->with("tambah_user","Daftar Berhasil, Silahkan Login untuk Melakukan Pemesanan!");
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
