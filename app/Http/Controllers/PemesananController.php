<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\Pemesanan;
use App\Models\Pembayaran;
use App\Models\Pengambilan;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\User;

class PemesananController extends Controller
{
    public function hal_pesan($id)
    {
        return view('pemesanan', [
            'title' => 'Pemesanan',
            'produk'=> Produk::find($id),
            'user'=> User::find($id),
            'pembayaran'=> Pembayaran::all(),
            'pengambilan'=> Pengambilan::all()
        ]);
    }

    public function tambah_pemesanan(Request $request)
    {
        $pemesanan = new Pemesanan;
        $pemesanan->user_id = $request->user_id;
        $pemesanan->produk_id = $request->produk_id;
        $pemesanan->pembayaran_id = $request->pembayaran_id;
        $pemesanan->pengambilan_id = $request->pengambilan_id;
        $pemesanan->tgl_pemesanan = $request->tgl_pemesanan;
        $pemesanan->jam_pengambilan = $request->jam_pengambilan;
        $pemesanan->jumlah = $request->jumlah;
        $pemesanan->alamat_kirim = $request->alamat_kirim;
        // Pastikan total_harga disimpan sebagai integer tanpa tanda koma
        $pemesanan->total_harga = intval(str_replace(',', '', $request->total_harga));

        $pemesanan->save();

        return redirect('/riwayat')->with("tambah_pemesanan", "Pemesanan berhasil ditambah. Silakan lakukan pembayaran!");
    }


    public function riwayat()
    {
        $pesan = Pemesanan::with(['user', 'produk', 'pengambilan', 'pembayaran'])->where('user_id', auth()->user()->id)->get();

        return view('riwayat', [
            'title' => 'Riwayat Pemesanan',
            'pesan' => $pesan,
        ]);
    }


    public function bukti($id, Request $request)
    {
        $request->validate([
            'bukti_bayar' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
         ]);

        $Pemesanan = Pemesanan::find($id);
        // dd();
        $Pemesanan->update($request->only(['bukti_bayar']));
        if($request->has(('bukti_bayar'))){
            $request->file('bukti_bayar')->move('assets/img/upload/', $request->file('bukti_bayar')->getClientOriginalName());
            $Pemesanan->bukti_bayar = $request->file('bukti_bayar')->getClientOriginalName();
            $Pemesanan->status = 'Menunggu Konfirmasi';
            $Pemesanan->save();
        }
        if ($Pemesanan->save()){
            return redirect()->back()->with('tambah_bukti', 'Bukti pembayaran berhasil diupload');
        }
    }

    public function delete_pemesanan($id)
    {
        Pemesanan::find($id)->delete();
        return redirect()->back()->with("delete_pemesanan","Pemesanan Berhasil di Hapus");
    }

    public function daftar_antrian()
    {
        $booking = Pemesanan::select('*')->whereIn('status',[ 'Belum Lunas', 'Menunggu Konfirmasi', 'Lunas'])->get();
        return view('admin/antrian',compact('booking'));
    }

    public function pesanankonfirmasi($id)
    {
        DB::table('pemesanans')->where('id',$id)->update(['status' => 'Lunas']);
        return redirect('/daftar_antrian')->with("update_pesan","Pesanan Berhasil Terkonfirmasi!");

    }
    public function pesananbatal($id)
    {
        DB::table('pemesanans')->where('id',$id)->delete();
        return redirect('/daftar_antrian')->with('delete_pesan', "Pemesanan Berhasil Dihapus!");
    }

    public function daftar_laporan()
    {
        $riwayat = Pemesanan::select('*')->where('status', 'Lunas')->get();
        $totalPemasukan = Pemesanan::where('status', 'Lunas')->sum('total_harga');
        return view('admin/laporan', compact('riwayat', 'totalPemasukan'));
    }

}
