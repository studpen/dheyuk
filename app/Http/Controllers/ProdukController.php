<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\Kategori;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ProdukController extends Controller
{
    public function menu(Request $request)
    {
        $kategoriId = $request->get('kategori_id');
        $query = Produk::query();

        if ($kategoriId) {
            $query->where('kategori_id', $kategoriId);
        }

        $menu = $query->get();
        $kategoris = Kategori::all();

        return view('menu', [
            'title' => 'Menu',
            'menu' => $menu,
            'kategoris' => $kategoris,
            'selectedKategori' => $kategoriId
        ]);
    }

    public function daftar_produk()
    {
        return view('admin/produk', [
            'title' => 'produk',
            'produk' => Produk::all(),
            'kategori' => Kategori::all()
        ]);
    }


    public function tambah_produk(Request $request)
    {
        $paket = Produk::create($request->except(['token', 'submit']));
        if($request->has(('gambar'))){
            $request->file('gambar')->move('assets/img/upload/', $request->file('gambar')->getClientOriginalName());
            $paket->gambar = $request->file('gambar')->getClientOriginalName();
            $paket->save();
        }
        if ($paket->save()) {
            return redirect('/produk')->with('tambah_produk', 'produk Berhasil Ditambah!');
        };
    }

    public function delete_produk($id)
    {
        Produk::find($id)->delete();
        return redirect()->back()->with("delete_produk","produk Berhasil di Hapus");
    }


    public function update_produk($id)
    {
        return view('admin/update/update_produk', [
            'title' => 'Update produk',
            'produk'=> Produk::find($id),
            'kategori'=> Kategori::all()
        ]);
    }

    public function edit_produk(Request $request)
    {
        // dd($request->file('gambar'));
        $paket = Produk::find($request->id);
        $paket->kategori_id = $request->kategori_id;
        $paket->nama_produk = $request->nama_produk;
        $paket->harga = $request->harga;
        $paket->keterangan = $request->keterangan;

        $request->validate([
            'gambar' => 'required|image|mimes:pjeg,png,jpg,gif,svg',
         ]);
        // Periksa apakah ada file gambar yang diunggah
        if ($request->has('gambar')) {
            // Hapus gambar lama jika ada
            if ($paket->gambar) {
                Storage::delete('assets/img/' . $paket->gambar);
            }

            $request->file('gambar')->move('assets/img/upload/', $request->file('gambar')->getClientOriginalName());
            $paket->gambar = $request->file('gambar')->getClientOriginalName();
        }

        if ($paket->save()) {
            return redirect('/produk')->with("edit_katalog", "Berhasil Diupdate!");
        } else {
            // Handle the case where the save fails
            return redirect('/produk')->with("edit_katalog", "Gagal Diupdate!");
        }
    }
}
