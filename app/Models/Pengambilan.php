<?php

namespace App\Models;

use App\Models\Produk;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pengambilan extends Model
{
    use HasFactory;

    protected $table = 'pengambilans';
    protected $guards = [];
    protected $fillable=['jenis_ambil'];

    public function pemesanan()
    {
        return $this->hasMany(Pemesanan::class, 'pengambilan_id', 'id');
    }
}
