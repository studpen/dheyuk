<?php

namespace App\Models;

use App\Models\User;
use App\Models\Produk;
use App\Models\Pembayaran;
use App\Models\Pengambilan;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pemesanan extends Model
{
    use HasFactory;

    protected $table = 'pemesanans';
    protected $guards = [];
    protected $fillable=['tgl_pemesanan','jumlah','total_harga','alamat_kirim'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id', 'id');
    }
    public function pembayaran()
    {
        return $this->belongsTo(Pembayaran::class, 'pembayaran_id', 'id');
    }
    public function pengambilan()
    {
        return $this->belongsTo(Pengambilan::class, 'pengambilan_id', 'id');
    }
}
