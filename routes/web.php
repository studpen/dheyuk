<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\PembayaranController;
use App\Http\Controllers\PemesananController;
use App\Http\Controllers\ProdukController;
use App\Models\Kategori;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::group(['middleware' => ['auth', 'CekRole:customer']], function () {
    Route::get('/', [HomeController::class,'index'])->name('index');
    Route::get('/layanan', [HomeController::class,'layanan']);
    Route::get('/menu', [ProdukController::class,'menu'])->name('menu');
    Route::get('/pembayaran', [PembayaranController::class,'pembayaran']);
    // pemesanan
    Route::get('/pemesanan/{id}', [PemesananController::class, 'hal_pesan'])->name('hal_pesan');
    Route::post('/tambah_pemesanan', [PemesananController::class, 'tambah_pemesanan'])->name('tambah_pemesanan');
    // data pemesanan
    Route::get('/riwayat', [PemesananController::class,'riwayat'])->name('riwayat');
    Route::get('/riwayat/{id}/delete', [PemesananController::class,'delete_pemesanan'])->name('delete_pemesanan');
    Route::post('/bukti/{id}', [PemesananController::class, 'bukti'])->name('bukti');   
    // info pembayaran
    Route::get('/pembayaran', [PembayaranController::class,'info_pembayaran'])->name('info_pembayaran');

    
});
Route::group(['middleware' => ['auth', 'CekRole:admin']], function () {
    Route::get('/admin', [AdminController::class,'dashboard'])->name('dashboard');

    // kategori
    Route::get('/kategori', [KategoriController::class,'daftar_kategori'])->name('daftar_kategori');
    Route::post('/tambah_kategori', [KategoriController::class,'tambah_kategori'])->name('tambah_kategori');
    Route::get('/kategori/{id}/edit', [KategoriController::class, 'update_kategori'])->name('update_kategori');
    Route::post('/kategori/{id}/update_kategori', [KategoriController::class, 'edit_kategori'])->name('edit_kategori');
    Route::get('/kategori/{id}/delete', [KategoriController::class, 'delete_kategori'])->name('delete_kategori');
    // produk
    Route::get('/produk', [ProdukController::class,'daftar_produk'])->name('daftar_produk');
    Route::post('/tambah_produk', [ProdukController::class,'tambah_produk'])->name('tambah_produk');
    Route::get('/produk/{id}/edit', [ProdukController::class, 'update_produk'])->name('update_produk');
    Route::post('/produk/{id}/update_produk', [ProdukController::class, 'edit_produk'])->name('edit_produk');
    Route::get('/produk/{id}/delete', [ProdukController::class, 'delete_produk'])->name('delete_produk');
    // antrian
    Route::get('/daftar_antrian', [PemesananController::class,'daftar_antrian'])->name('daftar_antrian');
    Route::get('/antrianedit/{id}', [PemesananController::class, 'pesanankonfirmasi'])->name('pesanankonfirmasi');
    Route::get('/antrianbatal/{id}', [PemesananController::class, 'pesananbatal'])->name('pesananbatal');
    // laporan
    Route::get('/daftar_laporan', [PemesananController::class,'daftar_laporan'])->name('daftar_laporan');
    
});
Route::get('/', [HomeController::class,'index'])->name('index');
Route::get('/layanan', [HomeController::class,'layanan'])->name('layanan');
Route::get('/menu', [ProdukController::class,'menu'])->name('menu');
Route::get('/pembayaran', [PembayaranController::class,'pembayaran'])->name('pembayaran');

Route::post('/login', [HomeController::class, 'login'])->name('login');
Route::post('/tambah_user', [HomeController::class, 'tambah_user'])->name('tambah_user');
Route::get('/logout', [HomeController::class, 'logout'])->name('logout');



