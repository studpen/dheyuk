<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dheyuk</title>
    <link rel="icon" href="{{ asset('assets/img/favicon.ico') }}" type="image/x-icon">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
</head>
<body>
    <!-- Navbar  -->
    @include('template.nav')

    <!-- Content -->
    <div class="container pt-4 mt-1">
        <h3 class="fw-bold pb-4">Daftar Menu</h5>
        <select class="form-select" id="filter-kategori" aria-label="Default select example">
            <option value="" selected>Semua jenis menu</option>
            @foreach ($kategoris as $kategori)
                <option value="{{ $kategori->id }}">{{ $kategori->nama_kategori }}</option>
            @endforeach
        </select>
        <div class="row mt-5">
            @foreach ($menu as $item)
                <div class="col-md-3 mb-3 mb-sm-0 menu-item pb-4" data-kategori="{{ $item->kategori_id }}">
                    <div class="card border-0 shadow-sm menu">
                        <img src="{{ asset('assets/img/upload/'.$item->gambar) }}" class="card-img-top" alt="...">
                        <div class="card-body" style="background-color: rgb(235, 231, 186)">
                            <h5 class="card-title fw-bold" style="color: rgb(38, 100, 40)">{{ $item->nama_produk }}</h5>
                            <p class="card-text fw-semibold fs-5 mb-0">{{ 'Rp '. number_format($item->harga, 0, ',', '.') }}</p>
                            @if (Auth::user())
                                @if (Auth::check() && Auth::user()->role == 'customer')
                                    <div class="d-flex justify-content-end">
                                        <a type="button" class="btn btn-success" href="/pemesanan/{{ $item->id }}">Pesan</a>
                                    </div>
                                @else
                                    <div class="d-flex justify-content-end">
                                        <a type="button" class="btn btn-success" href="/pemesanan_admin/{{ $item->id }}">Pesan</a>
                                    </div>
                                @endif
                            @endif
                            @if (!Auth::user())
                                <div class="row g-0 mt-3">
                                    <p class="mb-0 fw-semibold alert alert-danger" style="font-size: 16px;">Silahkan login dahulu!</p>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <!-- Footer -->
    @include('template.footer')

    <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
    <script>
    document.addEventListener('DOMContentLoaded', function () {
        const filterKategori = document.getElementById('filter-kategori');
        const menuItems = document.querySelectorAll('.menu-item');

        filterKategori.addEventListener('change', function () {
            const selectedKategori = this.value;
            menuItems.forEach(item => {
                const itemKategori = item.getAttribute('data-kategori');
                if (selectedKategori === "" || itemKategori === selectedKategori) {
                    item.style.display = 'block';
                } else {
                    item.style.display = 'none';
                }
            });
        });
    });
    </script>
</body>
</html>
