<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dheyuk</title>
    <link rel="icon" href="{{ asset('assets/img/favicon.ico') }}" type="image/x-icon">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
</head>
<body>
    <!-- Navbar  -->
    @include('template.nav')

    <!-- Content -->
    @if (session()->has('tambah_pemesanan'))
        <div class="alert alert-info alert-dismissible fade show" role="alert">
            {{ session('tambah_pemesanan') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    @if (session()->has('delete_pemesanan'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('delete_pemesanan') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    @if (session()->has('tambah_bukti'))
        <div class="alert alert-info alert-dismissible fade show" role="alert">
            {{ session('tambah_bukti') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="container pt-4 mt-1">
        <h3 class="fw-bold pb-4">Data Pemesanan</h5>
            <div class="container ms-8">
                <div class="table-responsive-xxl">
                    <table class="table">
                        <thead class="table-success">
                            <tr>
                                <th class="align-middle" style="color: rgb(38, 100, 40)">Nama</th>
                                <th class="align-middle" style="color: rgb(38, 100, 40)">Waktu Pemesanan</th>
                                <th class="align-middle" style="color: rgb(38, 100, 40)">Tanggal Pesan</th>
                                <th class="align-middle" style="color: rgb(38, 100, 40)">Jam Ambil</th>
                                <th class="align-middle" style="color: rgb(38, 100, 40)">Pengiriman</th>
                                <th class="align-middle" style="color: rgb(38, 100, 40)">Total Harga</th>
                                <th class="align-middle" style="color: rgb(38, 100, 40)">Payment</th>
                                <th class="align-middle" style="color: rgb(38, 100, 40)">Bukti Pembayaran</th>
                                <th class="align-middle" style="color: rgb(38, 100, 40)">Status</th>
                                <th class="align-middle" style="color: rgb(38, 100, 40)">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pesan as $index => $item)
                                @if($item->user_id == auth()->user()->id)
                                    <tr>
                                        <td>{{ $item->user->nama }}</td>
                                        <td>{{ $item->created_at }}</td>
                                        <td>{{ $item->tgl_pemesanan }}</td>
                                        <td>{{ $item->jam_pengambilan }}</td>
                                        <td>{{ $item->pengambilan->jenis_ambil }}</td>
                                        <td>Rp {{ number_format($item->total_harga, 0, ',', '.') }}</td>
                                        <td>
                                            @if ($item->pembayaran->nama_metode !== 'Bayar Langsung')
                                            {{ $item->pembayaran['nama_metode']}}<br>
                                            ({{ $item->pembayaran['no_rek']}})
                                            @else
                                            {{ $item->pembayaran['nama_metode']}}<br>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($item->bukti_bayar)
                                                <p>
                                                    <a href="#" class="link-offset-2 link-underline link-underline-opacity-0" data-bs-toggle="modal" data-bs-target="#lihat-gambar-{{ $item->id }}">
                                                        Bukti
                                                    </a>
                                                </p>
                                                <!-- Modal -->
                                                <div class="modal fade" id="lihat-gambar-{{ $item->id }}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content" style="background-color: transparent; border:0;">
                                                            <div class="modal-header" style="border-bottom:0;">
                                                                <button type="button" class="btn-close bg-white" data-bs-dismiss="modal" aria-label="Close"></button>

                                                            </div>
                                                            <div class="modal-body p-0 d-flex justify-content-center">
                                                                <img src="{{ asset('assets/img/upload/'.$item->bukti_bayar)}}" alt="{{ $item->bukti_bayar }}" class="img-fluid">

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </td>
                                        <td>
                                            {{-- <span class="badge text-bg-warning">{{ $item->status }}</span> --}}
                                            @if ($item->status == 'Belum lunas')
                                                <p class="mb-0 col-5"><span class="badge text-bg-danger fs-6">{{ $item->status }}</span></p>
                                            @endif
                                            @if ($item->status == 'Menunggu Konfirmasi')
                                                <p class="mb-0 col-5"><span class="badge text-bg-warning fs-6">{{ $item->status }}</span></p>
                                            @endif
                                            @if ($item->status == 'Lunas')
                                                <p class="mb-0 col-5"><span class="badge text-bg-success fs-6">{{ $item->status }}</span></p>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="container d-flex flex-column">
                                                <a type="button" class="btn btn-danger mb-1" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Batal pesan" href="/riwayat/{{ $item->id }}/delete" onclick="return confirm('Apakah yakin menghapus pesanan Anda?')">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">
                                                        <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5M8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5m3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0"/>
                                                    </svg>
                                                </a>
                                                @if ($item->pembayaran->nama_metode !== 'Bayar Langsung')
                                                    @if (!$item->bukti_bayar)
                                                    <button type="button" class="btn btn-success mb-1" data-bs-placement="top" data-bs-title="Upload bukti bayar" data-bs-toggle="modal" data-bs-target="#image-bukti-{{ $item->id }}">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cloud-arrow-up-fill" viewBox="0 0 16 16">
                                                            <path d="M8 2a5.53 5.53 0 0 0-3.594 1.342c-.766.66-1.321 1.52-1.464 2.383C1.266 6.095 0 7.555 0 9.318 0 11.366 1.708 13 3.781 13h8.906C14.502 13 16 11.57 16 9.773c0-1.636-1.242-2.969-2.834-3.194C12.923 3.999 10.69 2 8 2m2.354 5.146a.5.5 0 0 1-.708.708L8.5 6.707V10.5a.5.5 0 0 1-1 0V6.707L6.354 7.854a.5.5 0 1 1-.708-.708l2-2a.5.5 0 0 1 .708 0z"/>
                                                        </svg>
                                                    </button>
                                                    @endif
                                                @endif
                                            </div>
                                        </td>
                                    <tr>
                                    <div class="modal fade" id="image-bukti-{{ $item->id }}" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <form action="{{ route('bukti', ['id' => $item->id]) }}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Upload Bukti Pembayaran</h5>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="mb-3">
                                                            <input type="file" name="bukti_bayar" class="form-control shadow-none" accept="image/*">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn text-secondary shadow-none" data-bs-dismiss="modal">Kembali</button>
                                                        <button type="submit" class="btn btn-success text-white shadow-none">Kirim</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
    </div>

    <!-- Footer -->
    @include('template.footer')

    <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
</body>
</html>
