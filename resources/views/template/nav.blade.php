<!-- Navbar  -->
<nav class="navbar navbar-expand-lg sticky-top shadow-sm bg-body-tertiary p-2">
    <div class="container d-flex align-items-center">
        <a class="navbar-brand fw-bold d-flex align-items-center" href="{{ route('index')}}">
            <img src="{{ asset('assets/img/dheyuks.png') }}" alt="Logo" width="120" height="70"
                class="d-inline-block align-text-top">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mx-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link fw-medium" aria-current="page" href="{{ route('index')}}">Beranda</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link fw-medium" href="{{ route('menu')}}">Menu</a>
                </li>
                {{-- <li class="nav-item">
                    <a class="nav-link fw-medium" href="{{ route('layanan') }}">layanan</a>
                </li> --}}
                <li class="nav-item dropdown">
                    <a class="nav-link fw-medium dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                        aria-expanded="false">
                        Informasi
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="{{ route('layanan') }}">Pemesanan</a></li>
                        <li><a class="dropdown-item" href="{{ route('pembayaran') }}">Pembayaran</a></li>
                    </ul>
                </li>
                @if (Auth::user())
                    @if (Auth::check() && Auth::user()->role == 'customer')
                    {{-- Authenticated user --}}
                        <li class="nav-item">
                            <a class="nav-link fw-medium" href="{{ route('riwayat') }}">Data Pemesanan</a>
                        </li>
                    @endif
                @endif
            </ul>
            <ul class="navbar-nav mb-2 mb-lg-0 align-items-center">
                @if (!Auth::user())
                    <li class="nav-item">
                        <a class="btn btn-info border-0 px-3 py-2 m-1" data-bs-toggle="modal" data-bs-target="#exampleModal1" href="#">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-secondary border-0 px-3 py-2 m-1" data-bs-toggle="modal" data-bs-target="#exampleModal2" href="#">Daftar</a>
                    </li>
                @endif
                @if (Auth::user())
                    {{-- <li class="nav-item">
                        <a class="nav-link fw-medium icon-link-hover" href=" " style="--bs-icon-link-transform: translate3d(0, -.125rem, 0);">
                            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="#416D19" class="bi bi-cart4" viewBox="0 0 16 16">
                                <path d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0 1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5M3.14 5l.5 2H5V5zM6 5v2h2V5zm3 0v2h2V5zm3 0v2h1.36l.5-2zm1.11 3H12v2h.61zM11 8H9v2h2zM8 8H6v2h2zM5 8H3.89l.5 2H5zm0 5a1 1 0 1 0 0 2 1 1 0 0 0 0-2m-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0m9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2m-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0"/>
                            </svg>
                        </a>
                    </li> --}}
                    <li class="nav-item dropdown">
                        <a class="nav-link fw-medium  d-flex align-items-center ms-2" href="#" style="font-size: 1rem" role="button" data-bs-toggle="dropdown"
                        aria-expanded="false">
                            <img  src="{{ asset('assets/img/'.auth()->user()->gambar) }}" alt="Logo" width="34" height="34"
                                class="d-inline-block align-text-top bg-white rounded-circle">
                            {{ auth()->user()->nama }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link fw-medium d-flex align-items-center icon-link-hover" href="{{ route('logout') }}" style="--bs-icon-link-transform: translate3d(0, -.125rem, 0);">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#416D19" class="bi bi-box-arrow-right" viewBox="0 0 16 16">
                                <path fill-rule="evenodd" d="M10 12.5a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v2a.5.5 0 0 0 1 0v-2A1.5 1.5 0 0 0 9.5 2h-8A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-2a.5.5 0 0 0-1 0z"/>
                                <path fill-rule="evenodd" d="M15.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708.708L14.293 7.5H5.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708z"/>
                              </svg>
                            Logout
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>

<!-- Modal  Login-->
<div class="modal fade" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header" style="background-color: var(--green2);" data-bs-theme="primary">
        <h1 class="modal-title fs-4 fw-bold" id="exampleModalLabel" style="color: var(--lightgreen);">
            {{-- <img src="{{ asset('assets/img/normal_koperasi.png') }}" alt="Logo" width="36" height="36"
                class="d-inline-block align-text-top"> --}}
                Login
            </h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="{{ route('login') }}" method="POST">
            <div class="modal-body">
                @csrf
                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" name="email" class="form-control" id="email" placeholder="nama@gmail.com" value="{{ old('email') }}">
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" name="password" class="form-control" id="password">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success rounded">Login</button>
            </div>
        </form>
    </div>
    </div>
</div>


<!-- Modal  Daftar-->
<div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header" style="background-color: var(--green1);" data-bs-theme="dark">
        <h1 class="modal-title fs-4 fw-bold" id="exampleModalLabel" style="color: var(--lightgreen);">
                Daftar
            </h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="{{ route('tambah_user') }}" method="post">
            @csrf
            <div class="modal-body">
                <div class="mb-3">
                    <label for="nama" class="form-label">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Isi nama anda">
                </div>
                <div class="mb-3">
                    <label for="no_hp" class="form-label">No HP</label>
                    <input type="tel" class="form-control" id="no_tlp" name="no_tlp" placeholder="08xxxxxxxxxx">
                </div>
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Email</label>
                    <input type="email" class="form-control" name="email" id="exampleFormControlInput1" placeholder="nama@gmail.com">
                </div>
                <div class="mb-3">
                    <label for="inputPassword" class="form-label">Password</label>
                    <input type="password" class="form-control" name="password" id="inputPassword">
                </div>
                    <input type="text" class="form-control" name="role" id="role" value="customer" hidden>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-info rounded">Daftar</button>
            </div>
        </form>
    </div>
    </div>
</div>