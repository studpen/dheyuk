<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Admin | Laporan</title>
  <link rel="icon" href="{{ asset('assets/img/favicon.ico') }}" type="image/x-icon">

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('lte/dist/css/adminlte.min.css') }}">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
  integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

    @include('admin.template.nav_admin')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Antrian</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
            @if (session()->has('update_pesan'))
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                    {{ session('update_pesan') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            @if (session()->has('delete_pesan'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ session('delete_pesan') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <h3 class="card-title">Daftar Pemesanan</h3>
        </div>
        @if(count($riwayat))
        <div class="card-body p-3">
          <table class="table table-striped projects" id=dataTable>
                <thead>
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            Cust
                        </th>
                        <th>
                            Waktu Pesan
                        </th>
                        <th>
                            Tanggal Pesan
                        </th>
                        <th>
                            Jam Ambil
                        </th>
                        <th>
                            Alamat Kirim
                        </th>
                        <th>
                            Pengiriman
                        </th>
                        <th>
                            Pembayaran
                        </th>
                        <th>
                            Total Biaya
                        </th>
                    </tr>
                </thead>
                <tbody>
                @php
                    $counter = 1;
                @endphp
                    @foreach($riwayat as $item)
                    <tr>
                        <td>
                            {{ $counter++ }}
                        </td>
                        <td>
                            {{ $item->user['nama'] }}
                        </td>
                        <td>
                            {{  $item->created_at }}
                        </td>
                        <td>
                            {{ $item->tgl_pemesanan }}
                        </td>
                        <td>
                            {{ $item->jam_pengambilan }}
                        </td>
                        <td>
                            {{ $item->alamat_kirim }}
                        </td>
                        <td>
                            {{ $item->pengambilan->jenis_ambil }}
                        </td>
                        <td>{{ $item->pembayaran['nama_metode'] }}<br>
                            <img src="{{ asset('assets/img/upload/'.$item->bukti_bayar)}}" alt="{{ $item->bukti_bayar }}" class="img-fluid" width="100px">
                        </td>
                        <td>
                            {{ 'Rp '. number_format($item->total_harga, 0, ',', '.') }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="7" style="text-align:right"><strong>Total Seluruh Pemasukan:</strong></td>
                        <td id="totalPemasukan" colspan="2" class="text-end"><strong class="pe-4">Rp {{ number_format($totalPemasukan, 0, ',', '.') }}</strong></td>
                    </tr>
                </tfoot>
          </table>
        </div>
        @else
            <p class="text-center"> Belum Ada Pesanan</p>
        @endif
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('admin.template.footer_admin')


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{ asset('lte/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('lte/dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('lte/dist/js/demo.js') }}"></script>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
crossorigin="anonymous">
</script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
<script>
    $(document).ready(function () {
        $('#dataTable').DataTable();
    });
</script>
<script src="{{ asset('assets/js/main.js') }}"></script>




</body>
