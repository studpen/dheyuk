<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin Dheyuk| Dashboard</title>
    <link rel="icon" href="{{ asset('assets/img/favicon.ico') }}" type="image/x-icon">

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('lte/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('lte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('lte/dist/css/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/admin.css') }}">
</head>
<body class="hold-transition sidebar-mini layout-fixed" id="body">
<div class="wrapper">

  <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__wobble" src="{{ asset('assets/img/dheyuks.png') }}" height="60" width="100">
  </div>

  <!-- Main Sidebar Container -->
  @include('admin.template.nav_admin')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12 d-flex align-items-center justify-content-between">
                    <h1 class="m-0">Dashboard</h1>
                    <div class="mode-toggle">
                        <label class="switch">
                            <input type="checkbox" onclick="toggleMode()" id="modeSwitch">
                            <span class="slider round"></span>
                        </label>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
              <a class="info-box-icon bg-info elevation-1" href=""><i class="fas fa-hourglass-half"></i></a>

              <div class="info-box-content">
                <span class="info-box-text">Pesanan belum terverifikasi</span>
                <span class="info-box-number">
                    {{ $digunakan }}
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <!-- /.col -->

          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-3">
              <div class="info-box mb-3">
                  <a class="info-box-icon bg-success elevation-1" href=""><i class="fas fa-shopping-cart"></i></a>

                  <div class="info-box-content">
                      <span class="info-box-text">Pemasukan</span>
                      <span class="info-box-number">{{ 'Rp ' . number_format($income, 0, ',', '.')}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <div class="col-12 col-sm-6 col-md-3">
              <div class="info-box mb-3">
                  <a class="info-box-icon bg-danger elevation-1" href=""><i class="fas fa-users"></i></a>
                  <div class="info-box-content">
                    <span class="info-box-text">Customer</span>
                    <span class="info-box-number">{{ $customer }}</span>
                  </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <a class="info-box-icon bg-warning elevation-1" href=""><i class="fas fa-users"></i></a>

              <div class="info-box-content">
                <span class="info-box-text">Admin</span>
                <span class="info-box-number">{{ $admin }}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  @include('admin.template.footer_admin')

</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="{{ asset('lte/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('lte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('lte/dist/js/adminlte.js') }}"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="{{ asset('lte/plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('lte/plugins/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('lte/plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
<script src="{{ asset('lte/plugins/jquery-mapael/maps/usa_states.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('lte/plugins/chart.js/Chart.min.js') }}"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="{{ asset('lte/dist/js/demo.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('lte/dist/js/pages/dashboard2.js') }}"></script>

<script type="text/javascript">
    
    Highcharts.chart('grafik', {
        title : {
            text: "Grafik Penyewaan Perbulan"
        },
        xAxis : {
            categories : bulan
        },
        yAxis : {
            title: {
                text : "Nominal Pendapatan Bulanan"
            }
        },
        plotOptions : {
            series : {
                allowPointSelect: true
            }
        },
        series : [
        {
            name : "Nominal Pendapatan",
            data : pendapatan
        }
        ]
    });
</script>

<script>
    function toggleMode() {
  const body = document.getElementById('body');
  const modeSwitch = document.getElementById('modeSwitch');

  // Toggle the dark-mode class on the body based on the switch state
  body.classList.toggle('dark-mode', modeSwitch.checked);
  body.classList.toggle('light-mode', !modeSwitch.checked);
}
</script>
</body>
</html>
