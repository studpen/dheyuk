<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dheyuk</title>
    <link rel="icon" href="{{ asset('assets/img/favicon.ico') }}" type="image/x-icon">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">
    {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous"> --}}
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
</head>

<body>
    <!-- Navbar  -->
    @include('template.nav')

    <!-- Content -->
    <section class="container-fluid p-0 m-0">
        <div id="carouselExampleAutoplaying" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="{{ asset('assets/img/caro.png') }}" class="d-block w-100 h-100" alt="tumpeng">
              </div>
              <div class="carousel-item">
                <img src="{{ asset('assets/img/caro-1.png') }}" class="d-block w-100 h-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="{{ asset('assets/img/caro-2.png') }}" class="d-block w-100 h-100" alt="...">
              </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleAutoplaying" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleAutoplaying" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
            </button>
          </div>
    </section>
    <div class="container pt-5 mt-4">
        <h5 class="text-center fw-bold pt-4">VARIASI</h5>
        <h3 class="text-center fw-bold pb-4">Produk Andalan Kami</h5>
        <div class="row">
            @foreach ($kategori as $item)
            <div class="col-sm-4 mb-3 mb-sm-0" id="kategori">
                <div class="card border-0 shadow">
                    <img src="{{ asset('assets/img/upload/'.$item->gambar) }}" class="card-img-top" alt="...">
                    <div class="card-body" style="background-color: rgb(235, 231, 186)">
                        <h5 class="card-title fw-bold" style="color: rgb(38, 100, 40)">{{ $item->nama_kategori }}</h5>
                        <p class="card-text">{{ $item->deskripsi }}</p>
                        <a href="{{ route('menu') }}" class="btn btn-success" style="background-color: rgb(38, 100, 40)">Selengkapnya</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    <!-- Footer -->
    @include('template.footer')

    <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
</body>

</html>
