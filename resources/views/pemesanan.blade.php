<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dheyuk</title>
    <link rel="icon" href="{{ asset('assets/img/favicon.ico') }}" type="image/x-icon">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
</head>
<body>
    <!-- Navbar  -->
    @include('template.nav')

    <!-- Content -->
    <section class="container pt-4 mt-1">
            <h3 class="fw-bold pb-4">Pemesanan Catering</h5>
            <div class="container mx-auto mt-3 py-1">
                <div class="container p-0">
                    <div class="row g-0">
                        <div class="col-md-6">
                            <p class="fw-medium" style="font-size: 20px;">Data Pemesan</p>
                            <div class="container d-flex justify-content-start p-0">
                                <img src="{{ asset('assets/img/'.auth()->user()->gambar) }}" class="img-thumbnail m-0" alt="..." width="110" height="130">
                                <div class="container">
                                    <p class="fw-semibold fs-4 mb-0" style="font-size: 20px;">{{ auth()->user()->nama }}</p>
                                    <p class="mb-0">{{ auth()->user()->no_tlp }}</p>
                                    <p class="mb-0">{{ auth()->user()->email }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <p class="fw-medium" style="font-size: 20px;">Paket Sewa Yang Dipilih :</p>
                            <div class="container mx-0 px-0">
                                <div class="card mb-3 rounded-0"  style="background-color: rgb(235, 231, 186)">
                                    <div class="row g-0">
                                        <div class="col-md-6 p-3">
                                        <img src="{{ asset('assets/img/upload/'.$produk->gambar) }}" class="img-fluid" alt="...">
                                        </div>
                                        <div class="col-md-6 p-3">
                                        <div class="card-body p-0">
                                            <p class="fw-semibold fs-4 m-0">{{$produk->nama_produk}}</p>
                                            <p class="m-0">{{$produk->keterangan}}</p>
                                            <p class="mt-md-4 mb-md-0 fs-5 fw-bold text-end">{{ 'Rp '. number_format($produk->harga, 0, ',', '.') }}</p>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <hr class="container mx-auto">
    <div class="container scrollspy-example" data-bs-spy="scroll" data-bs-target="#simple-list-example" data-bs-offset="0" data-bs-smooth-scroll="true" tabindex="0">
        @if (Auth::check() && Auth::user()->role == 'customer')
        <form action="/tambah_pemesanan"  method="post">
        @else
        <form action="/tambah_pemesanan_admin"  method="post">
        @endif
            @csrf
                <input type="text" class="form-control" id="user_id" name="user_id" value="{{ auth()->user()->id }}" hidden >
                <input type="text" class="form-control" id="produk_id" name="produk_id" value="{{$produk->id}}" hidden>
            <div class="row mb-3">
                <label for="tgl_pemesanan" class="col-lg-2 col-form-label">Tanggal Pesan</label>
                <div class="col-lg-4">
                    <input type="date" class="form-control" id="tgl_pemesanan" name="tgl_pemesanan" required>
                </div>
            </div>
            <div class="row mb-3">
                <label for="jam_pengambilan" class="col-lg-2 col-form-label">Jam Pengambilan</label>
                <div class="col-lg-4">
                    <input type="time" class="form-control" id="jam_pengambilan" name="jam_pengambilan" required>
                </div>
            </div>
            <div class="row mb-3">
                <label for="jumlah" class="col-lg-2 col-form-label">Jumlah</label>
                <div class="col-lg-4">
                    <input type="number" class="form-control" name="jumlah" id="jumlah" step="1" value="1" min="1" required>
                </div>
            </div>
            <div class="row mb-3">
                <label for="total_harga" class="col-lg-2 col-form-label">Harga</label>
                <div class="col-lg-4">
                    <input type="number" name="total_harga" id="total_harga" class="form-control" readonly hidden>
                    <div id="total_harga_display" class="form-control" readonly></div>
                </div>
            </div>
            <div class="row mb-3">
                <label for="pengambilan_id" class="col-lg-2 col-form-label">Pengiriman</label>
                <div class="col-lg-4">
                    <select name="pengambilan_id" class="form-select form-select-lg mb-0" aria-label=".form-select-sm example" style="font-size: 16px" required>
                        <option selected hidden>Pilih Metode Pengiriman</option>
                        @foreach ($pengambilan as $p)
                            <option value="{{ $p->id }}">{{ $p->jenis_ambil }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row mb-3">
                <label for="alamat_kirim" class="col-lg-2 col-form-label">Alamat Kirim</label>
                <div class="col-lg-4">
                    <input type="text" class="form-control" name="alamat_kirim" id="alamat_kirim" aria-describedby="alamat_kirim" required>
                </div>
            </div>
            <div class="row mb-3">
                <label for="pembayaran_id" class="col-lg-2 col-form-label">Pembayaran</label>
                <div class="col-lg-4">
                    <select name="pembayaran_id" class="form-select form-select-lg mb-0" aria-label=".form-select-sm example" style="font-size: 16px" required>
                        <option selected hidden>Pilih Metode Pembayaran</option>
                        @foreach ($pembayaran as $p)
                            <option value="{{ $p->id }}">{{ $p->nama_metode }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <button type="submit" class="btn btn-success py-2 px-4 " onclick="return confirm('Apakah benar dengan reservasi Anda?')">Kirim</button>
            <a href="{{ route('menu')}}" class="btn btn-secondary py-2 px-4 ">Batal</a>
        </form>
    </div>

    <!-- Footer -->
    @include('template.footer')

    <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            const today = new Date();
            const tomorrow = new Date(today);
            tomorrow.setDate(tomorrow.getDate() + 1);
    
            const yyyy = tomorrow.getFullYear();
            const mm = String(tomorrow.getMonth() + 1).padStart(2, '0'); // Months are 0-based
            const dd = String(tomorrow.getDate()).padStart(2, '0');
    
            const minDate = `${yyyy}-${mm}-${dd}`;
            document.getElementById('tgl_pemesanan').setAttribute('min', minDate);
            document.getElementById('tgl_pemesanan').setAttribute('value', minDate);
    
            const jumlahInput = document.getElementById('jumlah');
            const hargaInput = document.getElementById('total_harga');
            const hargaProduk = {{ $produk->harga }}; // Ambil harga produk dari server
    
            function updateHarga() {
                const jumlah = parseInt(jumlahInput.value);
                const totalHarga = jumlah * hargaProduk;
                hargaInput.value = totalHarga;
                // Menampilkan harga dalam format IDR
                document.getElementById('total_harga_display').innerText = totalHarga.toLocaleString('id-ID');
            }
    
            // Set initial value
            updateHarga();
    
            // Event listener for change in jumlah input
            jumlahInput.addEventListener('input', function () {
                if (jumlahInput.value < 1) {
                    jumlahInput.value = 1;
                }
                updateHarga();
            });
        });
    </script>
    
</body>
</html>
