<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        DB::table('users')->insert([[
            "nama"  => "admin",
            "no_tlp" => "084134353122",
            "email" => "admin@gmail.com",
            "password" => Hash::make("admin"),
            "gambar" => "user-9.jpg",
            "role" => "admin",
          ],[
            "nama"  => "customer",
            "no_tlp" => "083847086323",
            "email" => "customer@gmail.com",
            "password" => Hash::make("customer"),
            "gambar" => "user-2.jpg",
            "role" => "customer",
          ]]);

          DB::table('pembayarans')->insert([[
            "nama_metode"  => "Bank BRI",
            "no_rek"  => "0026-01-00384-56-3"
          ],[
            "nama_metode"  => "DANA",
            "no_rek"  => "087897384678" 
          ],[
            "nama_metode"  => "Bayar Langsung",
            "no_rek"  => " ",
          ]]);

          DB::table('pengambilans')->insert([[
            "jenis_ambil"  => "Diantar Ke Tempat",
          ],[
            "jenis_ambil"  => "Diambil Sendiri",
          ]]);

          DB::table('kategoris')->insert([[
            "nama_kategori"  => "Tumpeng",
            "deskripsi"  => "Aneka jenis Nasi tumpeng kasi sajikan yang terbaik dengan berbagai lauk yang sesuai dengan selera anda",
            "gambar"  => "1.png"
          ],[
            "nama_kategori"  => "Nasik Kotak",
            "deskripsi"  => "Berbagai macam jenis nasi kotak kami sajikan dengan sangat baik dengan pilihan menu yang beraneka ragam",
            "gambar"  => "2.png"
          ],[
            "nama_kategori"  => "Jajanan",
            "deskripsi"  => "Kami menyediakan aneka jajanan rumahan yang menggugah selera dan tentunya cocok untuk hantaran acara anda",
            "gambar"  => "4.png"
          ]]);

          DB::table('produks')->insert([[
            "kategori_id"  => "1",
            "nama_produk"  => "Nasi Tummpeng Kuning",
            "harga" => "120000",
            "keterangan"  => "Tumpeng dengan nasi kuning ditambah dengan lauk ayam goreng, urap-urap, perkedel, oseng-oseng kentang, dan tempe bacem",
            "gambar"  => "1.png"
          ],
          [
            "kategori_id"  => "1",
            "nama_produk"  => "Nasi Tummpeng Putih",
            "harga" => "100000",
            "keterangan"  => "Tumpeng dengan nasi Putih ditambah dengan lauk ayam goreng, urap-urap, perkedel, oseng-oseng kentang, dan tempe bacem",
            "gambar"  => "7.png"
          ],
          [
            "kategori_id"  => "2",
            "nama_produk"  => "Ayam suwir",
            "harga" => "13000",
            "keterangan"  => "Nasi dengan ayam suwir ditambah dengan oseng tempe, sayur, dan sambel",
            "gambar"  => "2.png"
          ],
          [
            "kategori_id"  => "2",
            "nama_produk"  => "Ayam Geprek",
            "harga" => "12000",
            "keterangan"  => "Nasi dengan ayam geprek di tambah sambel",
            "gambar"  => "8.png"
          ],
          [
            "kategori_id"  => "2",
            "nama_produk"  => "Ayam Bakar",
            "harga" => "15000",
            "keterangan"  => "Nasi dengan ayam bakar di tambah sambel",
            "gambar"  => "9.png"
          ],
          [
            "kategori_id"  => "2",
            "nama_produk"  => "Ayam Krispi",
            "harga" => "13000",
            "keterangan"  => "Nasi kuning dengan ayam krispi di tambah dengan mie dan oseng kentang",
            "gambar"  => "10.png"
          ],
          [
            "kategori_id"  => "3",
            "nama_produk"  => "Klepon",
            "harga" => "1000",
            "keterangan"  => "Klepon dengan isi gula merah",
            "gambar"  => "4.png"
          ],
          [
            "kategori_id"  => "3",
            "nama_produk"  => "Risoles",
            "harga" => "2500",
            "keterangan"  => "Risoles dengan isi ayam",
            "gambar"  => "11.png"
          ],
          [
            "kategori_id"  => "3",
            "nama_produk"  => "Risol Mayo",
            "harga" => "3000",
            "keterangan"  => "Risol dengan isi sosis, keju dan mayones",
            "gambar"  => "5.png"
          ]
        ]);
    }
}
