<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pemesanans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users');
            $table->foreignId('produk_id')->constrained('produks');
            $table->foreignId('pembayaran_id')->constrained('pembayarans');
            $table->foreignId('pengambilan_id')->constrained('pengambilans');
            $table->date('tgl_pemesanan');
            $table->time('jam_pengambilan');
            $table->integer('jumlah')->nullable(); 
            $table->integer('total_harga'); 
            $table->string('alamat_kirim');
            $table->string('bukti_bayar')->nullable();
            $table->string('status')->default('Belum lunas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pemesanans');
    }
};
